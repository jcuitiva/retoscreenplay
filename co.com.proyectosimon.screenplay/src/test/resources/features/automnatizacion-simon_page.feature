# language:es
#Author: your.email@your.domain.com

@tag
Característica: Formulario de Registro  en Simon
	Como usuario
	Quiero Ingresar a la Pagina de simon
	A realizar el registro de una persona

@tag1
Escenario: Realizar el reporte de tiempo en MaxTime

	Dado que Jesus accede a la pagina de simon
	Cuando el realiza registro de sus datos en formulario
	|tipoPersona|tipoDocumento|numeroIdentificacion|nombres|apellidos|sexo|fechaNacimiento|clave|municipio|barrio|tipoAvenida|numeroTipoAvenida|numeroIntercepcion|numeroCasa|estrato|correo|telefono|
  |Persona Natural|Cédula de Ciudadanía|789456123|Jesus Alberto|Escobar Gaviria|Masculino|15121991|alberto123456|Medellín|Buenos Aires , Buenos aires|Carrera|30|30|20|3|cuitiva12121xx@hotmail.com|3205629214|
	Entonces  verifica que Los datos han sido registrados.

