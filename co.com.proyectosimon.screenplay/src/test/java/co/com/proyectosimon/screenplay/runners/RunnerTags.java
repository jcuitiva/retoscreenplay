package co.com.proyectosimon.screenplay.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features="src/test/resources/features/automnatizacion-simon_page.feature",
		tags= "@tag1",
		glue= {"co.com.proyectosimon.screenplay.stepdefinitions", "co.com.proyectosimon.screenplay.util"},
		snippets=SnippetType.CAMELCASE		)
public class RunnerTags {

}
