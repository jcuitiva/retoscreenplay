package co.com.proyectosimon.screenplay.stepdefinitions;

import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import static org.hamcrest.Matchers.equalTo;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

import java.util.List;

import org.apache.tools.ant.filters.LineContains.Contains;
import org.hamcrest.Matchers;

import co.com.proyectosimon.screenplay.model.DatosPersona;
import co.com.proyectosimon.screenplay.questions.LaRespuesta;
import co.com.proyectosimon.screenplay.tasks.Diligenciar;
import co.com.proyectosimon.screenplay.tasks.Ingresar;


public class SimonPageStepDefinitions {

	
	
	
	@Dado("^que (.*) accede a la pagina de simon$")
	public void queJesusAccedeALaPaginaDeSimon(String jesus)  {
		theActorCalled(jesus).wasAbleTo(Ingresar.ALaPaginaDeSimon());
	}


	@Cuando("^el realiza registro de sus datos en formulario$")
	public void elRealizaRegistroDeSusDatosEnFormulario(List<DatosPersona> datos){
		theActorInTheSpotlight().attemptsTo(Diligenciar.ElFormularioDeRegistro(datos));
	}

	@Entonces("^verifica que (.*)$")
	public void verificaQueLosDatosHanSidoRegistrados(String laRespuestaEsperada) {
		OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(LaRespuesta.es(), Matchers.containsString(laRespuestaEsperada)));
	}

	
	
}
