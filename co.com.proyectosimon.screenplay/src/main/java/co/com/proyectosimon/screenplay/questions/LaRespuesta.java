package co.com.proyectosimon.screenplay.questions;

import co.com.proyectosimon.screenplay.userinterface.RegistroDatosPaginaSimon;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class LaRespuesta implements Question<String>{

	@Override
	public String answeredBy(Actor actor) {
		
		return Text.of(RegistroDatosPaginaSimon.CONFIRMACION_DATOS_REGISTRADOS).viewedBy(actor).asString();
	}

	public static LaRespuesta es() {
		// TODO Auto-generated method stub
		return new LaRespuesta();
	}

}
