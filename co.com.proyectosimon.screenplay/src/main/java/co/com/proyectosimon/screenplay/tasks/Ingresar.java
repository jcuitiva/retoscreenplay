package co.com.proyectosimon.screenplay.tasks;

import co.com.proyectosimon.screenplay.userinterface.PaginaSimon;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Ingresar implements Task{

	private PaginaSimon paginaSimon;

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(paginaSimon));
		
	}

	public static Ingresar ALaPaginaDeSimon() {

		return Tasks.instrumented(Ingresar.class);
	}

}
