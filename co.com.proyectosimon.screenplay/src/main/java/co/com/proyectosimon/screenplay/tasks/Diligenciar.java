package co.com.proyectosimon.screenplay.tasks;

import java.util.List;

import co.com.proyectosimon.screenplay.model.DatosPersona;
import co.com.proyectosimon.screenplay.questions.SeleccionarLista;
import co.com.proyectosimon.screenplay.userinterface.RegistroDatosPaginaSimon;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Diligenciar implements Task{
	
	private List<DatosPersona> datos;
	
	public Diligenciar(List<DatosPersona> datos) {
		super();
		this.datos = datos;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
	actor.attemptsTo(Click.on(RegistroDatosPaginaSimon.CLICK_TIPO_PERSONA));		
	actor.attemptsTo(SeleccionarLista.Desde(RegistroDatosPaginaSimon.SELECCIONA_TIPO_PERSONA, datos.get(0).getTipoPersona().trim()));
	
	
	actor.attemptsTo(Click.on(RegistroDatosPaginaSimon.CLICK_TIPO_DOCUMENTO));
	actor.attemptsTo(SeleccionarLista.Desde(RegistroDatosPaginaSimon.SELECCIONA_TIPO_DOCUMENTO, datos.get(0).getTipoDocumento().trim()));
	actor.attemptsTo(Enter.theValue(datos.get(0).getNumeroIdentificacion()).into(RegistroDatosPaginaSimon.INGRESA_NUMERO_CEDULA));
	actor.attemptsTo(Enter.theValue(datos.get(0).getNombres()).into(RegistroDatosPaginaSimon.INGRESA_NOMBRES));
	actor.attemptsTo(Enter.theValue(datos.get(0).getApellidos()).into(RegistroDatosPaginaSimon.INGRESA_APELLIDOS));
	actor.attemptsTo(Click.on(RegistroDatosPaginaSimon.CLICK_TIPO_SEXO));
	actor.attemptsTo(SeleccionarLista.Desde(RegistroDatosPaginaSimon.SELECCION_SEXO, datos.get(0).getSexo().trim()));
	actor.attemptsTo(Click.on(RegistroDatosPaginaSimon.CLICK_FECHA));
	actor.attemptsTo(Click.on(RegistroDatosPaginaSimon.CLICK_FECHA));
	actor.attemptsTo(Enter.theValue(datos.get(0).getFechaNacimiento()).into(RegistroDatosPaginaSimon.INGRESA_FECHA_NACIMIENTO));
	actor.attemptsTo(Enter.theValue(datos.get(0).getClave()).into(RegistroDatosPaginaSimon.INGRESA_CLAVE));
	actor.attemptsTo(Enter.theValue(datos.get(0).getClave()).into(RegistroDatosPaginaSimon.CONFIRMA_CLAVE));
	actor.attemptsTo(Click.on(RegistroDatosPaginaSimon.CLICK_MUNICIPIO));
	actor.attemptsTo(SeleccionarLista.Desde(RegistroDatosPaginaSimon.SELECCIONA_MUNICIPIO, datos.get(0).getMunicipio().trim()));
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e) {
				e.printStackTrace();
	}
	actor.attemptsTo(Click.on(RegistroDatosPaginaSimon.SELECCIONA_TIPO_DIRECCION));
	actor.attemptsTo(Click.on(RegistroDatosPaginaSimon.CLICK_BARRIO));
	actor.attemptsTo(SeleccionarLista.Desde(RegistroDatosPaginaSimon.SELECCIONA_BARRIO, datos.get(0).getBarrio().trim()));
	actor.attemptsTo(Click.on(RegistroDatosPaginaSimon.CLICK_TIPO_AVENIDA));
	actor.attemptsTo(SeleccionarLista.Desde(RegistroDatosPaginaSimon.SELECCIONA_TIPO_AVENIDA, datos.get(0).getTipoAvenida().trim()));
	actor.attemptsTo(Enter.theValue(datos.get(0).getNumeroTipoAvenida()).into(RegistroDatosPaginaSimon.INGRESA_NUMERO_AVENIDA));
	actor.attemptsTo(Enter.theValue(datos.get(0).getNumeroIntercepcion()).into(RegistroDatosPaginaSimon.INGRESA_NUMERO_INTERCEPCION));
	actor.attemptsTo(Enter.theValue(datos.get(0).getNumeroCasa()).into(RegistroDatosPaginaSimon.INGRESA_NUMERO_CASA));
	actor.attemptsTo(Click.on(RegistroDatosPaginaSimon.CLICK_ESTRATO));
	actor.attemptsTo(SeleccionarLista.Desde(RegistroDatosPaginaSimon.SELECCIONA_ESTRATO, datos.get(0).getEstrato().trim()));
	actor.attemptsTo(Enter.theValue(datos.get(0).getCorreo()).into(RegistroDatosPaginaSimon.INGRESA_EMAIL));
	actor.attemptsTo(Enter.theValue(datos.get(0).getTelefono()).into(RegistroDatosPaginaSimon.INGRESA_TELEFONO_MOVIL));
	actor.attemptsTo(Click.on(RegistroDatosPaginaSimon.ACEPTA_POLICITA_HABEAS_DATA));
	actor.attemptsTo(Click.on(RegistroDatosPaginaSimon.ACEPTA_CONDICIONES));
	actor.attemptsTo(Click.on(RegistroDatosPaginaSimon.GUARDA_REGISTRO));
	
	
	
	
	
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e) {
				e.printStackTrace();
	}
		
	}
	
	public static Diligenciar ElFormularioDeRegistro(List<DatosPersona> datos) {
		
		return Tasks.instrumented(Diligenciar.class, datos);
	}



}
