package co.com.proyectosimon.screenplay.model;

public class DatosPersona {

	private String tipoPersona;
	private String tipoDocumento;
	private String numeroIdentificacion;
	private String nombres;
	private String apellidos;
	private String sexo;
	private String fechaNacimiento;
	private String clave;
	private String municipio;
	private String barrio;
	private String tipoAvenida;
	private String numeroTipoAvenida;
	private String numeroIntercepcion;
	private String numeroCasa;
	private String estrato;
	private String correo;
	private String telefono;
	
	
	
	
	public String getBarrio() {
		return barrio;
	}
	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}
	public String getTipoAvenida() {
		return tipoAvenida;
	}
	public void setTipoAvenida(String tipoAvenida) {
		this.tipoAvenida = tipoAvenida;
	}
	public String getNumeroTipoAvenida() {
		return numeroTipoAvenida;
	}
	public void setNumeroTipoAvenida(String numeroTipoAvenida) {
		this.numeroTipoAvenida = numeroTipoAvenida;
	}
	public String getNumeroIntercepcion() {
		return numeroIntercepcion;
	}
	public void setNumeroIntercepcion(String numeroIntercepcion) {
		this.numeroIntercepcion = numeroIntercepcion;
	}
	public String getNumeroCasa() {
		return numeroCasa;
	}
	public void setNumeroCasa(String numeroCasa) {
		this.numeroCasa = numeroCasa;
	}
	public String getTipoPersona() {
		return tipoPersona;
	}
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}
	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getEstrato() {
		return estrato;
	}
	public void setEstrato(String estrato) {
		this.estrato = estrato;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
}
