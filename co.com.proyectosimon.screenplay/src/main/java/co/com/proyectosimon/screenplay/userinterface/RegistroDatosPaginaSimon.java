package co.com.proyectosimon.screenplay.userinterface;

import org.openqa.selenium.By;

import net.serenitybdd.screenplay.targets.Target;

public class RegistroDatosPaginaSimon {
	
	public static final Target CLICK_TIPO_PERSONA = Target.the("seleccion tipo de persona").located(By.id("select2-chosen-15"));
	public static final Target SELECCIONA_TIPO_PERSONA = Target.the("seleccion tipo de persona").located(By.xpath("//ul[@id='select2-results-15']"));
	
	public static final Target CLICK_TIPO_DOCUMENTO = Target.the("seleccion tipo documento").located(By.id("select2-chosen-16"));
	public static final Target SELECCIONA_TIPO_DOCUMENTO = Target.the("seleccion tipo documento").located(By.xpath("//div[@id='select2-drop-mask' or @id='select2-drop'][2]"));
	public static final Target INGRESA_NUMERO_CEDULA = Target.the("ingresa # cedula").located(By.xpath("//*[@id=\"numeroidentificacion\"]"));
	public static final Target INGRESA_NOMBRES = Target.the("ingresa nombres completos").located(By.xpath("//*[@id=\"nombres\"]"));
	public static final Target INGRESA_APELLIDOS = Target.the("Ingresa apellidos completos").located(By.xpath("//*[@id=\"apellidos\"]"));
	public static final Target CLICK_TIPO_SEXO = Target.the("Ingresa apellidos completos").located(By.xpath("//*[@id=\"select2-chosen-17\"]"));
	
	public static final Target SELECCION_SEXO = Target.the("seleccion sexo").located(By.xpath("//ul[@id='select2-results-17']"));
	public static final Target CLICK_FECHA = Target.the("seleccion sexo").located(By.xpath("//input[@id='fechanacimiento']"));
	public static final Target INGRESA_FECHA_NACIMIENTO = Target.the("ingresa fecha nacimiento").located(By.xpath("//*[@id=\"fechanacimiento\"]"));
	public static final Target INGRESA_CLAVE = Target.the("ingresa contraseña").located(By.xpath("//*[@id=\"clave_uno\"]"));
	public static final Target CONFIRMA_CLAVE = Target.the("confirma contraseña").located(By.xpath("//*[@id=\"clave_dos\"]"));
	
	public static final Target CLICK_MUNICIPIO = Target.the("confirma contraseña").located(By.xpath("//span[@id='select2-chosen-19']"));	
	public static final Target SELECCIONA_MUNICIPIO = Target.the("selecciona municipio").located(By.xpath("/html[1]/body[1]/div[7]"));
	
	public static final Target SELECCIONA_TIPO_DIRECCION = Target.the("selecciona tipo direccion").located(By.xpath("//div[@id='formulario_registro_direccionOcomuna']//div[2]//ins[1]"));
	public static final Target CLICK_BARRIO = Target.the("selecciona tipo direccion").located(By.xpath("//span[@id='select2-chosen-20']"));
	public static final Target SELECCIONA_BARRIO = Target.the("selecciona tipo direccion").located(By.xpath("//ul[@id='select2-results-20']"));
	public static final Target CLICK_TIPO_AVENIDA = Target.the("selecciona tipo direccion").located(By.xpath("//div[@id='s2id_formulario_registro_direccion_format_tipo_via']//b[@role='presentation']"));
	
	public static final Target SELECCIONA_TIPO_AVENIDA = Target.the("selecciona tipo direccion").located(By.xpath("//ul[@id='select2-results-22']"));
	public static final Target INGRESA_NUMERO_AVENIDA = Target.the("selecciona tipo direccion").located(By.xpath("//input[@id='formulario_registro_direccion_format_numero_via']"));
	public static final Target INGRESA_NUMERO_INTERCEPCION = Target.the("selecciona tipo direccion").located(By.xpath("//input[@id='formulario_registro_direccion_format_numero_generador']"));
	public static final Target INGRESA_NUMERO_CASA = Target.the("selecciona tipo direccion").located(By.xpath("//input[@id='formulario_registro_direccion_format_numero_placa']"));
	
	public static final Target CLICK_ESTRATO = Target.the("selecciona tipo direccion").located(By.xpath("//span[@id='select2-chosen-28']"));
	public static final Target SELECCIONA_ESTRATO = Target.the("seleccion estratp").located(By.xpath("//ul[@id='select2-results-28']"));
	public static final Target INGRESA_EMAIL = Target.the("ingresa email").located(By.xpath("//input[@id='correoelectronico']"));
	public static final Target INGRESA_TELEFONO_MOVIL = Target.the("ingresa # movil").located(By.xpath("//input[@id='telefonomovil']"));
	public static final Target ACEPTA_POLICITA_HABEAS_DATA = Target.the("confirma policita").located(By.xpath("//*[@id=\"formatoAlto2\"]/div/div[3]/div/ins"));
	public static final Target ACEPTA_CONDICIONES = Target.the("acepta terminos y condiciones").located(By.xpath("//*[@id=\"formatoAlto2\"]/div/div[7]/div/ins"));
	public static final Target GUARDA_REGISTRO = Target.the("Click boton guardar").located(By.xpath("//*[@id=\"registro_save\"]"));
	public static final Target CONFIRMACION_DATOS_REGISTRADOS = Target.the("Validacion datos registrados").located(By.xpath("//div[@class='alert alert-success alert-dismissable']"));
	
}
